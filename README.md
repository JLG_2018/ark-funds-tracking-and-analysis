This project is to track and analyze the fund investment record of ARK.

### ARKK: ARK Innovation ETF

ARK defines ‘‘disruptive innovation’’ as the introduction of a technologically enabled new product or service that potentially changes the way the world works.

Companies within ARKK include those that rely on or benefit from the development of new products or services, technological improvements and advancements in scientific research relating to the areas of DNA technologies (‘‘Genomic Revolution”), industrial innovation in energy, automation and manufacturing (‘‘Industrial Innovation’’), the increased use of shared technology, infrastructure and services (‘‘Next Generation Internet’), and technologies that make financial services more efficient (‘‘Fintech Innovation’’).

ARKK is an actively managed ETF that seeks long-term growth of capital by investing under normal circumstances primarily (at least 65% of its assets) in domestic and foreign equity securities of companies that are relevant to the Fund’s investment theme of disruptive innovation.

### ARKQ: ARK Autonomous Technology & Robotics ETF
Companies within ARK Autonomous Technology & Robotics ETF* (ARKQ) are focused on and are expected to substantially benefit from the development of new products or services, technological improvements and advancements in scientific research related to, among other things, energy, automation and manufacturing, materials, and transportation. These companies may develop, produce, or enable:

* Autonomous Transportation
* Robotics and Automation
* 3D Printing
* Energy Storage
* Space Exploration
ARKQ is an actively managed ETF that seeks long-term growth of capital by investing under normal circumstances primarily (at least 80% of its assets) in domestic and foreign equity securities of of autonomous technology and robotics companies that are relevant to the Fund’s investment theme of disruptive innovation.

### ARKW: ARK Next Generation Internet ETF
Companies within the ARK Next Generation Internet ETF* (ARKW) are focused on and expected to benefit from shifting the bases of technology infrastructure to the cloud, enabling mobile, new and local services, such as companies that rely on or benefit from the increased use of shared technology, infrastructure and services, internet-based products and services, new payment methods, big data, the internet of things, and social distribution and media. These companies may develop, produce or enable:

* Cloud Computing & Cyber Security
* E-Commerce
* Big Data & Artificial Intelligence (AI)
* Mobile Technology and Internet of Things
* Social Platforms
* Blockchain & P2P
ARKW is an actively managed ETF that seeks long-term growth of capital by investing under normal circumstances primarily (at least 80% of its assets) in domestic and U.S. exchange traded foreign equity securities of companies that are relevant to the Fund’s investment theme of next generation internet.

### ARKF: ARK FINTECH INNOVATION ETF
ARKF is an actively managed Exchange Traded Fund (ETF) that seeks long-term growth of capital. It seeks to achieve this investment objective by investing under normal circumstances primarily (at least 80% of its assets) in domestic and foreign equity securities of companies that are engaged in the Fund’s investment theme of financial technology (“Fintech”) innovation.

A company is deemed to be engaged in the theme of Fintech innovation if (i) it derives a significant portion of its revenue or market value from the theme of Fintech innovation, or (ii) it has stated its primary business to be in products and services focused on the theme of Fintech innovation. The Adviser defines “Fintech innovation” as the introduction of a technologically enabled new product or service that potentially changes the way the financial sector works, which ARK believes includes but is not limited to the following business platforms:

* Transaction Innovations
* Blockchain Technology
* Risk Transformation
* Frictionless Funding Platforms
* Customer Facing Platforms
* New Intermediaries

### ARKG: ARK Genomic Revolution ETF
Companies within ARKG are focused on and are expected to substantially benefit from extending and enhancing the quality of human and other life by incorporating technological and scientific developments and advancements in genomics into their business. The companies held in ARKG may develop, produce or enable:

* CRISPR
* Targeted Therapeutics
* Bioinformatics
* Molecular Diagnostics
* Stem Cells
* Agricultural Biology
ARKG will be concentrated in issuers in any industry or group of industries in the health care sector, including issuers having their principal business activities in the biotechnology industry.

ARKG is an actively managed ETF that seeks long-term growth of capital by investing under normal circumstances primarily (at least 80% of its assets) in domestic and foreign equity securities of companies across multiple sectors, including health care, information technology, materials, energy and consumer discretionary, that are relevant to the Fund’s investment theme of the genomics revolution.