# -*- coding: utf-8 -*-
"""
Created on Fri Dec 11 13:22:16 2020

@author: JLG_2018

This file is created to update and process the CSV files for storing ARK investment history.
"""

import pandas as pd
try:
    from urllib.request import Request, urlopen  # Python 3
except ImportError:
    from urllib2 import Request, urlopen  # Python 2
   
url_ARKK='https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_INNOVATION_ETF_ARKK_HOLDINGS.csv'
ARKK_csv='ARKK.csv'

url_ARKQ='https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_AUTONOMOUS_TECHNOLOGY_&_ROBOTICS_ETF_ARKQ_HOLDINGS.csv'
ARKQ_csv='ARKQ.csv'

url_ARKW='https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_NEXT_GENERATION_INTERNET_ETF_ARKW_HOLDINGS.csv'
ARKW_csv='ARKW.csv'

url_ARKF='https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_FINTECH_INNOVATION_ETF_ARKF_HOLDINGS.csv'
ARKF_csv='ARKF.csv'

url_ARKG='https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_GENOMIC_REVOLUTION_MULTISECTOR_ETF_ARKG_HOLDINGS.csv'
ARKG_csv='ARKG.csv'

def idx_buy_max(df,group_atri='company',max_atri='DayChg(%)'):
    '''
    This function is to find the indexes with maximum value for the shares bought for a certain group.
    group_atri: The name in string format for grouping the dataframe. Default value is 'company'.
    max_atri: The name in string format for finding the maximum value. Default value is 'DayChg(%)'.
    '''
    idx=(df.groupby(group_atri)[max_atri].transform(max)==df[max_atri]) \
    & (df.groupby(group_atri)[max_atri].transform(max)>0)
    return idx

def share_change(df, group_atri='company', change_atri='shares', periods='NaN', start_date='11/19/2020'):
    '''
    This function is to do basic analysis on the investment history. Calculating the change of one parameter in one singel day 
    or in one defined period.
    group_atri: The name in string format for grouping the dataframe. Default value is 'company'.
    change_atri: The name in string format for calculating the change on. Default value is 'shares'.
    periods: Defined period in number of business days to calculate the change.
    start_date: The starting date of selecting the dataframe to show.
    '''
    df['DayChg']=df.groupby(group_atri)[change_atri].diff()
    df['DayChg(%)']=df.groupby(group_atri)[change_atri].pct_change()*100
    if periods!='NaN':
        df['PeriodsChg']=df.groupby(group_atri)[change_atri].diff(periods=periods)
        df['PeriodsChg(%)']=df.groupby(group_atri)[change_atri].pct_change(periods=periods)*100
    idx=df.date>=start_date
    return df[idx]

def ARK_fund_update(url,csvfile):
    '''
    This function is to update the daily investment record through reading the online CSV file published by ARK.
    url: the url link of the csv file online.
    csvfile: the csv file stored locally.
    '''
    req = Request(url)
    req.add_header('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0')
    content = urlopen(req)
    df=pd.read_csv(content)
    df.drop(df[df.fund.isnull()].index, inplace=True)
    if df.date[0] not in pd.read_csv(csvfile,usecols=['date']).date.tolist():
        df.to_csv(csvfile, mode='a', header=False, index=False)
    return
 
ARK_fund_update(url_ARKQ,ARKQ_csv)
ARK_fund_update(url_ARKK,ARKK_csv)
ARK_fund_update(url_ARKW,ARKW_csv)
ARK_fund_update(url_ARKF,ARKF_csv)
ARK_fund_update(url_ARKG,ARKG_csv)